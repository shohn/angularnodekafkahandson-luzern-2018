import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ChartComponent } from 'angular2-highcharts/index';
import { ChartService } from './chart.services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ChartService]
})
export class AppComponent implements OnInit, OnDestroy {

  messages = [];
  subscriptions = [];
  options: Object;
  @ViewChild('chartVar') refObj: any;

  constructor(private chartService: ChartService) { }

  ngOnInit() {
    this.renderChart();
    this.subscriptions.push(this.chartService.getLiveData1().subscribe(message => {
      this.messages.push(message);
      message['y'] = +message['y'];
      this.refObj.chart.series[0].addPoint({ x: message['x'], y: null }, false);
      this.refObj.chart.series[1].addPoint(message, false);
      this.refObj.chart.redraw();
    }));
    this.subscriptions.push(this.chartService.getLiveData2().subscribe(message => {
      this.messages.push(message);
      message['y'] = +message['y'];
      this.refObj.chart.series[0].addPoint({ x: message['x'], y: null }, false);
      this.refObj.chart.series[2].addPoint(message, false);
      this.refObj.chart.redraw();
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  renderChart() {
    this.options = {
      rangeSelector: {
        inputEnabled: false,
        buttons: [{
          type: 'all',
          text: 'All'
        }],
        selected: 0
      },

      title: {
        text: 'Live Data from Kafka'
      },
      xAxis: {
        opposite: true
      },
      yAxis: {
        opposite: true
      },
      exporting: {
        enabled: false
      },

      series: [{
        name: '0',
        data: []
      },
      {
        name: 'Live data 1',
        data: []
      },
      {
        name: 'Live data 2',
        data: []
      }]

    };
  }

}
