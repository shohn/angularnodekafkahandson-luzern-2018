import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { ChartModule } from 'angular2-highcharts';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';
import { ChartService } from './chart.services';

declare var require: any;
export function highchartsFactory() {
  const hc = require('highcharts');
  hc.setOptions({
    global: {
      timezoneOffset: -120
    }
  });
  const dd = require('highcharts/modules/drilldown');
  const ex = require('highcharts/modules/exporting');
  const st = require('highcharts/modules/stock');

  dd(hc);
  ex(hc);
  st(hc);
  return hc;
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ChartModule
  ],
  providers: [{ provide: HighchartsStatic, useFactory: highchartsFactory }],
  bootstrap: [AppComponent]
})
export class AppModule { }
